% CODE TO CARRY OUT MFM PERTURBATION ON CAMCAN DATASET FOR COMPUTATION OF
% NET INFLUENCE AND NODE LEVEL FLOW
%------------------------------Initialization------------------------------
load('avgSC150.mat')        % Loads the structural connectivity matrix
N = 150;					%Number of nodes
tau_s = 100;
gamma = 0.641/1000;
sigma = 0;
a = 270;
b = 108;
d = 0.154;
w = 0.9;
J = 0.2609;
I = 0.3;
ts = 1;						% Timestep (in ms)
T = 60000;					% Total duration (in ms)
G = [0.1,0.12,0.2,0.3,0.4];			% Values of scaling parameter at which simulations have to be run (informed by FCD distribution (MFM.m) and fixed point analysis (MFM_fixedpoint_analysis.m))

Nsim = 5;					% Total number of simulations
SS = [];
tic
for g = 1:length(G)
    for trial = 1:Nsim
        [G(g),trial]
        S = zeros(T/ts,N);
        S(1,:) = rand(1,N);   %Initial values of S for all the nodes
        H = zeros(1,N);
        x = zeros(1,N);
        S_interim = zeros(1,N);
        C = avgSC150;
        c = 1;
        %---------------------------Simulation---------------------------------
        for t = ts:ts:T
            t;
            for i = 1:N
                s = C(i,:)*S(c,:)';
                x = w*J*S(c,i) + G(g)*J*s + I;
                H = (a*x-b)/(1-exp(-d*(a*x-b)));
                S_interim(i) = S(c,i) + ts*((1-S(c,i))*gamma*H+sigma*randn-S(c,i)/tau_s);
            end
            S(c+1,:) = S_interim;
            c=c+1;
        end
        SS(:,trial,g)=S(end,:)';		% Steady-state values of the system
    end
end

C = avgSC150;
A = SS;
deg = sum(C);
%------------------------------Perturbation Protocol---------------------------
T = 5000;		% Duration of perturbation protocol
dn = -0.1;		% perturbation strength

R_MFM_G_camcan = zeros(N,N,length(G));
F_MFM_camcan = zeros(N,Nsim,length(G));
for g = 1:length(G)
    R_MFM_trial = zeros(N,N,Nsim);
    C = avgSC150;
    deg = sum(C);
    SS = A;
    for trial = 1:Nsim
        [g,trial]
        for n = 1:N
            S = zeros(T/ts,N);
            S(1,:) = SS(:,trial,g)';		% Initialise values to original steady state
            S(1,n) = (1+dn)*S(1,n);		% Perturbation of node n
            S_interim = zeros(1,N);
            S_interim(1,n) = S(1,n);		% Implementation of constant perturbation
            c = 1;
            x = [];
            H = [];
            %---------------------Perturbation Simulation----------------------
            for t = ts:ts:T
                t;
                for i = 1:N
                    if i~=n             	%Effect of perturbation on all nodes other than n
                        s = 0;
                        for j = 1:N
                            s = s + C(i,j)*S(c,j);
                        end
                        x = w*J*S(c,i) + G(g)*J*s + I;
                        H = (a*x-b)/(1-exp(-d*(a*x-b)));
                        S_interim(i) = S(c,i) + ts*(((1-S(c,i))*gamma*H)+(sigma*randn)-(S(c,i)/tau_s));
                    end
                end
                S(c+1,:) = S_interim;
                c=c+1;
            end
            SSPert=S(end,:)';			% Perturbed steady state
            for m = 1:N
                if m~=n
                    R_MFM_trial(m,n,trial) = abs((SSPert(m)-SS(m,trial,g))/(SS(m,trial,g)*dn));		% Response matrix calculation
                end
            end
        end
        for m=1:N
            if deg(m)==0
                R_MFM_trial(m,:,trial)=0;
                R_MFM_trial(:,m,trial)=0;
            end
        end
        
        Ri_app=zeros(N,N,N);
        for i = 1:N
            i;
            for m = 1:N
                if m ~= i
                    for n = 1:N
                        if n ~= i
                            Ri_app(m,n,i)=R_MFM_trial(m,n,trial)-R_MFM_trial(m,i,trial)*R_MFM_trial(i,n,trial);		% Approx. Response matrix with 'i' functionally lesioned
                        end
                    end
                end
            end
        end
        for i = 1:N
            Fi = zeros(1,N);
            for n=1:N
                if n~=i
                    Fi(n)=(sum(R_MFM_trial(:,n,trial))-sum(Ri_app(:,n,i)))/sum(R_MFM_trial(:,n,trial));
                    if isnan(Fi(n))
                        Fi(n)=0;
                    end
                end
            end
            F_MFM_camcan(i,trial,g)=(1/N)*sum(Fi);				% Flow of node i
        end
    end
    R_MFM_G_camcan(:,:,g)=mean(R_MFM_trial,3);
end
F_MFM_camcan_mean = [];
for i = 1:length(G)
    F_MFM_camcan_mean(:,i) = mean(F_MFM_camcan(:,:,i),2);                           % Mean flow per node across trials for G(i)
end
I_MFM_camcan = [];
for i = 1:length(G)
    I_MFM_camcan(:,i)=sum(R_MFM_G_camcan(:,:,i),1)'-sum(R_MFM_G_camcan(:,:,i),2);   % Net influence per node for G(i)
end
toc

figure;
for i = 1:length(G)                                     % Plot net influence as a function of node strength
    subplot(1,length(G),i)
    scatter(deg',I_MFM_camcan(:,i));
    ylabel('I');
    xlabel('Node strength')
    title(sprintf('G = %f',G(i)))
end
figure;
for i = 1:length(G)                                     % Plot flow as a function of node strength
    subplot(1,length(G),i)
    scatter(deg',F_MFM_camcan_mean(:,i));
    ylabel('Flow');
    xlabel('Node strength')
    title(sprintf('G = %f',G(i)))
end
