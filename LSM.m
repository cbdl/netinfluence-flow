% CODE TO CARRY OUT LSM PERTURBATION ON CAMCAN DATASET TO SYNTHESIZE
% RESTING STATE FUNCTIONAL CONNECTIVITY AND COMPARE IT WITH EMPIRICAL FC TO
% ARRIVE AT MODEL PARAMETERS THAT MIMIC EMPIRICAL OBSERVATIONS
%---------Initialization------------------
load('avgSC150.mat');
load('avgFC150.mat');
N = 150				% Number of nodes
ts = 1				% Timestep (in ms)
T = 315200			% Total duration (in ms)
G = 0:0.01:0.28			% Global scaling parameter
downsampBOLD = [];
Nsim = 30			% Total number of trials
FCDGsweep = cell(3,Nsim);
tic
for trial = 1:Nsim
    downsampBOLD = [];
    for g = 1:length(G)
        [trial,g]
        S = zeros(T/ts,N);
        S(1,:) = rand(1,N);
        S_interim = [];
        C = avgSC150;		% Structural connectivity (150 nodes)
        c = 1;
        sigma=0.28-G(g)		% Noise level
        %-------------------Simulation---------------------
        for t = ts:ts:T
            t;
            for i = 1:N
                s = C(i,:)*S(c,:)';
                S_interim(i) = S(c,i) + ts*(-S(c,i) + G(g)*s + sigma*randn);
            end
            S(c+1,:) = S_interim;
            c=c+1;
        end
        
        %--Conversion to BOLD and downsampling with TR=1.97s--
        simBOLD = [];
        for i = 1:N
            simBOLD = [simBOLD BOLDs(315.200,0.001,S(:,i))];
            downsampBOLD(:,i,g)=simBOLD(1:1970:end,i);
        end
    end
    FCDGsweep{1,trial} = downsampBOLD;
    zdownsampBOLD = [];
    for k = 1:size(downsampBOLD,3)
        zdownsampBOLD(:,:,k) = zscore(downsampBOLD(:,:,k));	% z-score downsampled BOLD
    end
    simFC = [];
    for k = 1:size(downsampBOLD,3)
        simFC(:,:,k)=corr(zdownsampBOLD(:,:,k));
    end
    FCD = [];
    FCD(:,1) = G';
    for k = 1:size(downsampBOLD,3)	% Compute the distance between simulated and empirical FC
        s = rs_FC-simFC(:,:,k);
        s = s.^2;
        FCD(k,2) = (1/N)*sqrt(sum(s(:)));   % Functional Connectivity Distance (FCD) for a model with G(k)
    end
    FCDGsweep{2,trial} = simFC;
    FCDGsweep{3,trial} = FCD;
end
FCD = [];
for i = 1:Nsim
    FCD = [FCD FCDGsweep{3,i}(:,2)];        % Functional Connectivity Distance (FCD) over G (rows) and trials (columns)
end
%save('sim_rest_CC150_LSM.mat','G','FCDGsweep','FCD','ts','T','-v7.3')
toc