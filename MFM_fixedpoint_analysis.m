% CODE TO CARRY OUT NOISELESS FIXED POINT ANALYSIS OF MEAN FIELD MODEL
%---------Initialization------------------
load('avgSC150.mat');

% %----------Uncomment if using NKI-------------
% N = 188;
% C = load('NKI_dti_avg_connectivity_matrix_file.txt');
% C = C./max(C(:));
% G = 0:0.005:0.2;

%---------Uncomment if using CamCAN-----------
N = 150;
C = avgSC150;
G = 0:0.01:0.5;

tau_s = 100;
gamma = 0.641/1000;
sigma = 0;
a = 270;
b = 108;
d = 0.154;
w = 0.9;
J = 0.2609;
I = 0.3;
ts = 1;
T = 40000;
N_sim = 10;

%---------------------HIGH IC FIXED POINTS---------------------------------
FP_mfm_high = [];
FP_mfm_low = [];
S = [];
for g = 1:length(G)
    G(g)
    max_f_rate = [];
    S_interim = [];
    H = [];
    x = [];
    S = 0.3+0.7*rand(N_sim,N);  %High ICs
    %---------Simulation----------------------
    for k = 1:N_sim
        [g,k]
        for t = ts:ts:T
            t;
            for i = 1:N
                s = C(i,:)*S(k,:)';
                x(k,i) = w*J*S(k,i) + G(g)*J*s + I;
                H(k,i) = (a*x(k,i)-b)/(1-exp(-d*(a*x(k,i)-b)));
                S_interim(i) = S(k,i) + ts*(((1-S(k,i))*gamma*H(k,i))+(sigma*randn)-(S(k,i)/tau_s));
            end
            S(k,:) = S_interim;
        end
        max_f_rate(k) = max(H(k,:));
    end
    FP_mfm_high = [FP_mfm_high max_f_rate(:)];
end

%----------------------LOW IC FIXED POINTS---------------------------------
for g = 1:length(G)
    G(g)
    max_f_rate = [];
    S_interim = [];
    H = [];
    x = [];
    S = 0.01*rand(N_sim,N);  %Low ICs
    %---------Simulation----------------------
    for k = 1:N_sim
        [g,k]
        for t = ts:ts:T
            t;
            for i = 1:N
                s = C(i,:)*S(k,:)';
                x(k,i) = w*J*S(k,i) + G(g)*J*s + I;
                H(k,i) = (a*x(k,i)-b)/(1-exp(-d*(a*x(k,i)-b)));
                S_interim(i) = S(k,i) + ts*(((1-S(k,i))*gamma*H(k,i))+(sigma*randn)-(S(k,i)/tau_s));
            end
            S(k,:) = S_interim;
        end
        max_f_rate(k) = max(H(k,:));
    end
    FP_mfm_low = [FP_mfm_low max_f_rate(:)];
end

figure;
hold on
for i = 1:10
    plot(G,FP_mfm_high(i,:),'LineStyle','none','Marker','square');
    plot(G,FP_mfm_low(i,:),'LineStyle','none','Marker','o','MarkerEdgeColor','k');
end
xlabel('G')
ylabel('max firing rate (Hz)')

deg = sum(C);
figure;                % Display node strength distribution
hist(deg)
xlabel('Node strength');
ylabel('Count');