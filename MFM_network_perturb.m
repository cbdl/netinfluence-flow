% CODE TO CARRY OUT MFM PERTURBATION ON CAMCAN DATASET FOR COMPUTATION OF
% NETWORK LEVEL FLOW
%------------------------------Initialization------------------------------
load('avgSC150.mat')
N = 150;			% Number of nodes
tau_s = 100;
gamma = 0.641/1000;
sigma = 0;
a = 270;
b = 108;
d = 0.154;
w = 0.9;
J = 0.2609;
I = 0.3;
ts = 1;				% Timestep (in ms)
T = 60000;			% Total duration (in ms)
G = [0.1,0.12,0.3];			% Input value of global scaling parameter [0.1,0.12,0.3] (informed by FCD distribution (MFM.m) and fixed point analysis (MFM_fixedpoint_analysis.m))

Nsim = 1;			% Number of simulations to be run
SS = [];
tic
for g = 1:length(G)
    for trial = 1:Nsim
        [G(g),trial]
        S = zeros(T/ts,N);
        S(1,:) = rand(1,N);   %Initial values of S for all the nodes
        H = zeros(1,N);
        x = zeros(1,N);
        S_interim = zeros(1,N);
        C = avgSC150;
        c = 1;
        %---------------------------Simulation---------------------------------
        for t = ts:ts:T
            t;
            for i = 1:N
                s = C(i,:)*S(c,:)';
                x = w*J*S(c,i) + G(g)*J*s + I;%+ impulse(c,i);
                H = (a*x-b)/(1-exp(-d*(a*x-b)));
                S_interim(i) = S(c,i) + ts*(((1-S(c,i))*gamma*H)+(sigma*randn)-(S(c,i)/tau_s));
            end
            S(c+1,:) = S_interim;
            c=c+1;
        end
        SS(:,trial,g)=S(end,:)';		% Steady-state values of the system
    end
end

C = avgSC150;
A = SS;
deg = sum(C);
%------------------------------Perturbation Protocol--------------------------
T = 5000;		% Duration of perturbation protocol
dn = -0.1;		% perturbation strength
network= cell(1,7);
network{1} = [12,15,16,48,53,54,57,63,67,73,80,89,90,91,99,123,128,130,132,138,140,142];                  %con
network{2} = [5,6,9,10,13,14,25,30,34,38,40,55,56,66,71,72,74,81,84,85,88,100,105,113,131,146,147];       %dmn
network{3} = [7,17,18,26,39,42,47,49,50,82,83,87,92,93,101,110,111,114,115,117,122,124,125,129];          %snva
network{4} = [27,61,68,69,70,102,104,112,136,143,144,145,148];                                            %da
network{5} = [2,11,19,20,21,22,43,45,52,58,59,60,62,77,86,94,95,96,97,118,120,127,133,134,135,137,141];   %vis
network{6} = [3,4,8,28,29,33,36,41,46,75,78,79,103,108,109,116,121,149,150];                              %smot
network{7} = [1,23,24,31,32,35,37,44,51,64,65,76,98,106,107,119,126,139];                                 %lim

R_MFM_camcan = zeros(N,N,length(G));
F_MFM_camcan_network = zeros(length(network),length(G),Nsim);
for g = 1:length(G)
    R_MFM_trial = zeros(N,N,Nsim);
    C = avgSC150;
    deg = sum(C);
    SS = A;
    for trial = 1:Nsim
        [g,trial]
        for n = 1:N     %sources
            S = zeros(T/ts,N);
            S(1,:) = SS(:,trial,g)';
            S(1,n) = (1+dn)*S(1,n); %perturbation
            S_interim = zeros(1,N);
            S_interim(1,n) = S(1,n);
            c = 1;
            x = [];
            H = [];
            %---------------------Perturbation Simulation----------------------
            for t = ts:ts:T
                for i = 1:N     %targets
                    if i~=n             %Effect of perturbation on all nodes other than n(source)
                        s = C(i,:)*S(c,:)';
                        x = w*J*S(c,i) + G(g)*J*s + I;
                        H = (a*x-b)/(1-exp(-d*(a*x-b)));
                        S_interim(i) = S(c,i) + ts*(((1-S(c,i))*gamma*H)+(sigma*randn)-(S(c,i)/tau_s));
                    end
                end
                S(c+1,:) = S_interim;
                c=c+1;
            end
            SSPert=S(end,:)';   %Perturbed steady state
            for m = 1:N
                if m~=n
                    R_MFM_trial(m,n,trial) = abs((SSPert(m)-SS(m,trial,g))/(SS(m,trial,g)*dn));
                end
            end
        end
        for m=1:N
            if deg(m)==0
                R_MFM_trial(m,:,trial)=0;
                R_MFM_trial(:,m,trial)=0;
            end
        end
        % %-----------------Exact Response Matrix with network 'net' frozen----------------------
        Ri=zeros(N,N,length(network));
        for net = 1:length(network)         %Loop over each of the 7 networks
            for n = 1:N     %Sources
                [net,n]
                if sum(n~=network{net})/length(network{net})==1     %Only nodes other than the frozen nodes are considered as sources
                    S = zeros(T/ts,N);
                    S(1,:) = SS(:,trial,g)';
                    S(1,n) = (1+dn)*SS(n,trial,g); %perturbation
                    S_interim = zeros(1,N);
                    S_interim(1,n) = S(1,n);
                    for f = network{net}
                        S_interim(1,f) = S(1,f);
                    end
                    c = 1;
                    x = [];
                    H = [];
                    %---------------------Perturbation Simulation----------------------
                    for t = ts:ts:T
                        for i = 1:N     %Targets
                            if i~=n && sum(i~=network{net})/length(network{net})==1 %Effect of perturbation on all nodes other than n(source) and network nodes
                                s = C(i,:)*S(c,:)';
                                x = w*J*S(c,i) + G(g)*J*s + I;
                                H = (a*x-b)/(1-exp(-d*(a*x-b)));
                                S_interim(i) = S(c,i) + ts*(((1-S(c,i))*gamma*H)+(sigma*randn)-(S(c,i)/tau_s));
                            end
                        end
                        S(c+1,:) = S_interim;
                        c=c+1;
                    end
                    SSPert=S(end,:)';       %Perturbed steady state
                    for m = 1:N
                        if m~=n
                            Ri(m,n,net) = abs((SSPert(m)-SS(m,trial,g))/(SS(m,trial,g)*dn));    %{mn}th element of the response matrix with network "net" frozen
                        end
                    end
                end
            end
        end
        for i = 1:length(network)
            Fi = zeros(1,N);
            for n=1:N
                if sum(n~=network{net})/length(network{net})==1
                    Fi(n)=(sum(R_MFM_trial(:,n,trial))-sum(Ri(:,n,i)))/sum(R_MFM_trial(:,n,trial));
                    if isnan(Fi(n))
                        Fi(n)=0;
                    end
                end
            end
            F_MFM_camcan_network(i,g,trial)=(1/length(network{i}))*sum(Fi);
        end
    end
    R_MFM_camcan(:,:,g)=mean(R_MFM_trial,3);
end
F_MFM_camcan_network_mean = mean(F_MFM_camcan_network,3);
toc
figure;
h=bar(F_MFM_camcan_network_mean);
name = {'Con';'DMN';'SNVA';'DA';'Vis';'SMot';'Lim'};
set(gca,'xticklabel',name);
set(h,{'DisplayName'},{'G=0.1','G=0.12','G=0.3'}');     % Change labels to values of scaling parameter used
legend()