% CODE TO CARRY OUT LSM PERTURBATION ON CAMCAN DATASET FOR COMPUTATION OF
% NET INFLUENCE AND NODE LEVEL FLOW
%---------Initialization------------------
load('avgSC150.mat')
N = 150;		% Number of nodes
ts = 1;			% Timestep (in ms)
T = 60000;		% Total duration (in ms)
G = [0.1,0.19,0.22];	% Values of scaling parameter at which simulations have to be run 
Nsim = 5;		% Total number of simulations
sigma = 0;
SS = [];
tic
for g=1:length(G)
    for trial = 1:Nsim
	[G(g),trial]
        C = avgSC150;
        S = zeros(T/ts,N);
        S(1,:) = rand(1,N);
        S_interim = [];
        c = 1;
        %---------------------------Simulation---------------------------------
        for t = ts:ts:T
            t;
            for i = 1:N
                s = C(i,:)*S(c,:)';
                S_interim(i) = S(c,i) + ts*(-S(c,i) + G(g)*s + sigma*randn);
            end
            S(c+1,:) = S_interim;
            c=c+1;
        end
        SS(:,trial,g)=S(end,:)';	% Steady-state of the system
    end
end
SS(SS==0)=1e-60;			% fix steady states to a non-zero value to avoid division by zero during response calculation
%------------------------------Perturbation Protocol---------------------------
T = 5000;		% Duration of perturbation protocol		
dn = 0.1;		% perturbation strength

R_LSM_G_camcan = zeros(N,N,length(G));
F_LSM = zeros(N,Nsim,length(G));
deg = sum(C);
for g = 1:length(G)
    R_LSM_trial = zeros(N,N,Nsim);
    for trial = 1:Nsim
        sigma = 0;%0.39 - G(g);
        for n = 1:N
            [trial,n]
            S = zeros(T/ts,N);
            S(1,:) = SS(:,trial,g)';		% Intialise values to original steady state
            S(1,n) = (1+dn)*S(1,n);		% Perturbation of node n
            S_interim = zeros(1,N);
            S_interim(1,n) = S(1,n);		% Implementation of constant perturbation
            c = 1;
            C = avgSC150;
            %------------------Perturbation Simulation----------------------
            for t = ts:ts:T
                for i = 1:N
                    if i~=n             % Effect of perturbation on all nodes other than n
                        s = C(i,:)*S(c,:)';
                        S_interim(i) = S(c,i) + ts*(-S(c,i) + G(g)*s + sigma*randn);
                    end
                end
                S(c+1,:) = S_interim;
                c=c+1;
            end
            SSPert=S(end,:)';			% Perturbed steady state
            for m = 1:N
                if m~=n
                    R_LSM_trial(m,n,trial) = abs((SSPert(m)-SS(m,trial,g))/(SS(m,trial,g)*dn));    %make SS(m,trialnum) if there are many trials
                end
            end
        end
        for m=1:N
            if deg(m)==0
                R_LSM_trial(m,:,trial)=0;
                R_LSM_trial(:,m,trial)=0;
            end
        end
        
        Ri_app=zeros(N,N,N);
        for i = 1:N
            i;
            for m = 1:N
                if m ~= i
                    for n = 1:N
                        if n ~= i
                            Ri_app(m,n,i)=R_LSM_trial(m,n,trial)-R_LSM_trial(m,i,trial)*R_LSM_trial(i,n,trial);		% Approx. Response matrix with 'i' functionally lesioned
                        end
                    end
                end
            end
        end
        for i = 1:N
            Fi = zeros(1,N);
            for n=1:N
                if n~=i
                    Fi(n)=(sum(R_LSM_trial(:,n,trial))-sum(Ri_app(:,n,i)))/sum(R_LSM_trial(:,n,trial));
                    if isnan(Fi(n))
                        Fi(n)=0;
                    end
                end
            end
            F_LSM(i,trial,g)=(1/N)*sum(Fi);			% Flow of node i
        end
    end
    R_LSM_G_camcan(:,:,g)=mean(R_LSM_trial,3);
end
F_LSM_camcan_mean = [];
for i = 1:length(G)
    F_LSM_camcan_mean(:,i) = mean(F_LSM(:,:,i),2);		% Mean flow across trials for G(i)
end
I_LSM_camcan = [];
for i = 1:length(G)
    I_LSM_camcan(:,i)=sum(R_LSM_G_camcan(:,:,i),1)'-sum(R_LSM_G_camcan(:,:,i),2);   % Net influence per node for G(i)
end
toc

figure;
for i = 1:length(G)                                     % Plot net influence as a function of node strength
    subplot(1,length(G),i)
    scatter(deg',I_LSM_camcan(:,i));
    ylabel('I');
    xlabel('Node strength')
    title(sprintf('G = %f',G(i)))
end
figure;
for i = 1:length(G)                                     % Plot flow as a function of node strength
    subplot(1,length(G),i)
    scatter(deg',F_LSM_camcan_mean(:,i));
    ylabel('Flow');
    xlabel('Node strength')
    title(sprintf('G = %f',G(i)))
end